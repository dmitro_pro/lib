<?php
require_once  "../includes/config.php";
?>

<?php include "../admin/includes/head.php" ?>
<?php session_start() ?>
<?php if(isset($_SESSION['admin'])) { ?>

</head>
<body>

 
<h2>Автор</h2>

<?php session_start(); ?>
<?php include "../admin/login.php" ?>
<br /> <br />

<section class="admin_bookslist">

    <? $autor = mysqli_query($connection, "SELECT * FROM autors WHERE id = " . (int) $_GET['edit_id']);  //подключаем выбираем таблицу books в id равном по запросу гет																снова: выбрали таблицу, потом к WHERE id приравняли запрос параметра гет

    if(mysqli_num_rows($autor) <= 0) // если страниц меньше 1 то


    {
    ?>




    <section class="pattern">
        <section class="row">
            <section class="page_name">
                <h1><a href="/"><?php echo $config['title']; ?></a></h1>
            </section>
        </section>
    </section>
    <? echo include "includes/sidebar.php" ?>

    <section class="content">
        <h4>Автор не найден</h4>


    </section>


</section>
</section>

<?

} else

{
    $autorpage = mysqli_fetch_assoc($autor);  	// извлекаем данные
    $fn = $autorpage['img_autor']; // имя с файлом картинки
    ?>






    <form enctype="multipart/form-data" method="POST" action="/admin/form_autor.php?edit_id=<? echo $autorpage['id'] ?>">

        <input type="text" placeholder="" name="name_autor" value="<? echo $autorpage['name_autor'] ?>" /><br /><br />
        <textarea placeholder="" value="" name="biography" style="height: 300px;"><? echo $autorpage['biography'] ?></textarea><br /><br />
        <input type="hidden" name="MAX_FILE_SIZE" value="3000000" />
        <input type="file" name="upload"><br />
        <input type="submit" name="edit_id" value="Сохранить" >

    </form>




    <?php //обработка загрузки фотографии

    if (isset($_POST['edit_id']))
    {
        // Перезапишем переменные для удобства
        $filePath  = $_FILES['upload']['tmp_name'];
        $errorCode = $_FILES['upload']['error'];
// Проверим на ошибки
        if ($errorCode !== UPLOAD_ERR_OK || !is_uploaded_file($filePath)) {
            // Массив с названиями ошибок
            $errorMessages = [
                UPLOAD_ERR_INI_SIZE   => 'Размер файла превысил значение upload_max_filesize в конфигурации PHP.',
                UPLOAD_ERR_FORM_SIZE  => 'Размер загружаемого файла превысил значение MAX_FILE_SIZE в HTML-форме.',
                UPLOAD_ERR_PARTIAL    => 'Загружаемый файл был получен только частично.',
                UPLOAD_ERR_NO_FILE    => 'Файл не был загружен.',
                UPLOAD_ERR_NO_TMP_DIR => 'Отсутствует временная папка.',
                UPLOAD_ERR_CANT_WRITE => 'Не удалось записать файл на диск.',
                UPLOAD_ERR_EXTENSION  => 'PHP-расширение остановило загрузку файла.',
            ];
            // Зададим неизвестную ошибку
            $unknownMessage = 'При загрузке файла произошла неизвестная ошибка.';
            // Если в массиве нет кода ошибки, скажем, что ошибка неизвестна
            $outputMessage = isset($errorMessages[$errorCode]) ? $errorMessages[$errorCode] : $unknownMessage;
            // Выведем название ошибки
            die($outputMessage);
        }
// Создадим ресурс FileInfo
        $fi = finfo_open(FILEINFO_MIME_TYPE);
// Получим MIME-тип
        $mime = (string) finfo_file($fi, $filePath);
// Закроем ресурс
        finfo_close($fi);
// Проверим ключевое слово image (image/jpeg, image/png и т. д.)
        if (strpos($mime, 'image') === false) die('Можно загружать только изображения.');
// Результат функции запишем в переменную
        $image = getimagesize($filePath);
// Зададим ограничения для картинок
        $limitBytes  = 1024 * 1024 * 5;
        $limitWidth  = 2000;
        $limitHeight = 2000;
// Проверим нужные параметры
        if (filesize($filePath) > $limitBytes) die('Размер изображения не должен превышать 5 Мбайт.');
        if ($image[1] > $limitHeight)          die('Высота изображения не должна превышать 768 точек.');
        if ($image[0] > $limitWidth)           die('Ширина изображения не должна превышать 1280 точек.');
// Сгенерируем новое имя файла на основе MD5-хеша
        $namefile = md5_file($filePath);
// Сгенерируем расширение файла на основе типа картинки
        $extension = image_type_to_extension($image[2]);
// Сократим .jpeg до .jpg
        $format = str_replace('jpeg', 'jpg', $extension);
// Переместим картинку с новым именем и расширением в папку
        //die(__DIR__ . '/upload/images/autors/' . $namefile . $format);


        // !!! __DIR__ не обязательно, нам надо загрузить в папку upload, которая в корне скрипта, а не в папке admin
        if (!move_uploaded_file($filePath, '../upload/images/autors/' . $namefile . $format)) {
            die('При записи изображения на диск произошла ошибка.');
        }
// чтобы передать в базу 2 переменные через одну


        // !!! Тут нужно правильно сформировать имя файла
        $filedb = $namefile . $format;
        $fn = $filedb; // тут присваиваем имя нового файла к переменной картинки


    }
    ?>




    <section class="container-content">
        <section class="container-content-w">
            <? echo include "includes/sidebar.php" ?>

            <section class="content">

                <section class="imgbook">

                    <img src="/upload/images/autors/<? echo $fn; ?>">		<!-- фото -->


                </section>

                <section class="desc">

                    <h4><? echo $autorpage['name_autor'] ?></h4>	<!-- имя страницы -->

                    <p><strong>Описание книги</strong></p>
                    <p><? echo $autorpage['biography'] ?></p>	<!-- описание -->


                    <br /><br />


                </section>

                <section style="clear: both;"> </section>
            </section>

            <section style="clear: both;"> </section>
        </section>
    </section>

    <?php


}

?>



/// пишем в базу

<?php


if (isset($_POST['edit_id']))
{


    $errors = array();

    if ($_POST['name_autor'] == '')
    {
        $errors[] = 'Введите имя автора!';
    }

    if ($_POST['biography'] == '')
    {
        $errors[] = 'Введите биографию!';
    }


    if ( empty($errors) )
    {
        // добавить комментари
        # echo "UPDATE books ( name_book , description_book , id ,edit_id) VALUES ('".$_POST['name_book']."', upload = '".$_POST['img_autor']."','".$_POST['description_book']."' ,'".$bookpage['id']."' , NOW(), '".$_POST['edit_id']."')";  // выводил чтобы проверить данные

        //exit();  // закрыть соединение

        // !!! А тут две ошибки было: 1 - неправильное имя поля с именем картинки, 2 - почему ты имя пытался брать из POST?
        mysqli_query($connection, "UPDATE autors set name_autor = '".$_POST['name_autor']."', img_autor = '".$filedb."'  , biography = '".$_POST['biography']."' WHERE id = ".$_GET['edit_id']."");
        echo '<span style="color:green; font-weight: bold; display:block;">Автор отредактирован!</span>';
    } else // в ином случае выводим текст ошибки
    {
        // вывести ошибку

        echo '<span style="color:red; font-weight: bold; display:block;">' .$errors['0']. '</span>';
    }

}


#$books = mysqli_query($connection, "REPLACE INTO books set name_book = '".$_POST['name_book']."', description_book = '".$_POST['description_book']."', id = '".$_POST['edit_id']."'");



?>
</body>
</html>


<?php } else { ?>
    Доступ запрещен!
<?php } ?>