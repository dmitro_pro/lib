
<?php
require_once  "../includes/config.php";
?>

<?php include "../admin/includes/head.php" ?>


<?php session_start(); ?>



<?php if(isset($_SESSION['admin'])) { ?>


    <h1><a href="/admin/admin.php">Администраторская панель электронной библиотеки</a></h1>
    <nav class="">
        <ul>
            <li><a href="/admin/list_genre.php">Жанры</a></li>
            <li><a href="/admin/list_book.php">Книги</a></li>
            <li><a href="/admin/list_autor.php">Авторы</a></li>

        </ul>
    </nav>

<?php } else { ?>

    <form style="width: 300px;
    margin: 65px auto;
    background: #fff;
    padding: 25px;
    border-radius: 25px;
    box-shadow: 5px 5px 5px 5px #f1f1f1;" action="/admin/login.php" method="POST" class="form-inline my-2 my-lg-0">
        <h1 class="h3 mb-3 font-weight-normal">Авторизация</h1>
    <strong>Введите логин</strong><br />
    <input class="form-control mr-sm-2" type="login" value="<?php echo $data['login'] ?>" placeholder="" name="login" /><br />
    <strong>Введите пароль</strong><br />
    <input class="form-control mr-sm-2"  type="password" value="<?php echo $data['password'] ?>" placeholder="" name="password" /><br /><br />
        <div class="g-recaptcha" data-sitekey="6LcKM2cUAAAAAChDan_mWpkiylTpyGAjn_nbvOft"></div>
        <!-- элемент для вывода ошибок -->
        <div class="text-danger" id="recaptchaError"></div>
        <button  class="btn btn-outline-success my-2 my-sm-0" type="submit">Войти</button>


</form>


    
<?php } ?>


<?php
session_start();

if (isset($_POST['login']) and isset($_POST['password'])) {
    $login = $_POST['login'];
    $password = $_POST['password'];

    if   ($login == "") {   // если стрим(очистка мусора пробелов) логин пусто, то выводим ошибку
        echo "<br />";
        echo "<div style='color: red;font-weight:bold;'>Введите логин</div>";
        echo "<br />";
    }
    if  ($password == "") { // если пароль пусто, то выводим ошибку - у пароля стрима нет т.к. его на пробелы не чистим
        echo "<div style='color: red;font-weight:bold;'>Введите ваш пароль</div>";
    }

    $query = "SELECT * FROM admins WHERE login = '$login' and password = '$password'";
    $result = mysqli_query($connection, $query) or die(mysqli_error($connection));
    $count = mysqli_num_rows($result);

    if ($count == 1) {
        $_SESSION['admin'] = $login;

    } else {
        $fmsg = "Ошибка";
    }
}



?>

<div style="width:300px;margin: 0 auto; text-align: center;">
<?php


if(isset($_SESSION['admin'])) {
    $login = $_SESSION['admin'];
    echo "Привет! " . $login. "";
    echo " Вы авторизованы, как администратор";
    echo " <a href='/admin/logout.php'>Выйти</a>";
}

?>
</div>