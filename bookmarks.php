<?php
session_start();
require_once  "includes/config.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $config['title']; ?></title>
    <meta name="viewport" content="width=device-width">
    <?php include "includes/head.php" ?>


</head>
<body>

<?php include "includes/header.php" ?>

<section class="pattern">
    <section class="row">
        <section class="page_name">
            <h1><a href="/"><?php echo $config['title']; ?></a></h1>
        </section>
    </section>
</section>

<?php session_start() ?>
<?php if(isset($_SESSION['login'])) { ?>

<section class="container-content">
    <section class="container-content-w">
        <?php  include "includes/sidebar.php"; ?>


        <section class="content">

            <!-- обработчик -->
            <?php
            $bookmarks = mysqli_query($connection, "SELECT * FROM   bookmarks bm LEFT JOIN users u ON u.id = bm.user_id LEFT JOIN books b ON bm.book_id = b.id"); /// подключились к таблице букмаркс

            /*
                        $books = mysqli_query($connection, "SELECT * FROM books"); // подключимся к таблице букс
                        $books_id = mysqli_fetch_assoc($books); // извлекли айди книги

                        $users_data = mysqli_query($connection, "SELECT * FROM users");
                        $users_data_id = mysqli_fetch_assoc($users_data);
            */
            ?>


            <?php  /// $delete = обработчик
            if (isset($_GET['del_id'])) { //проверяем, есть ли переменная
                $bookmarkq = mysqli_query($connection,"DELETE  FROM bookmarks WHERE id = ".$_GET['del_id']); //удаляем строку из таблицы
            }




            ?>

            <section class="latest_books">
                <h4>Список закладок</h4>
                <ul>
                    <?php while ( $bookmark = mysqli_fetch_assoc($bookmarks) ) { ?> <!--извлекаем данные в цикл while-->
                        <li>
                           <?php $bookmarks_connect = mysqli_query($connection, "SELECT * FROM bookmarks");
                           $bookmarks_id = mysqli_fetch_assoc($bookmarks_connect);
                           ?>
                            <section class="imagebg">
                                <a href="/book.php?id=<? echo $bookmark['id']; ?>"><img src="/upload/images/books/<? echo $bookmark['img_book']; ?>" /></a>
                            </section>

                            <section class="name_book"> <a href="/book.php?id=<? echo $bookmark['id']; ?>"><?php echo $bookmark['name_book']; ?></a> </section>




                            <section class="delete">
                                <a onclick="if(confirm('Удалить книгу из закладок?')){alert('Книга удалена!'); return true}else{return false}" href="?del_id=<?php echo $bookmarks_id['id']; ?>">Удалить из закладок</a> <!-- //передаем айди в базу на удаление,
            взял гет передачу, а ниже обрабатывает это все отправляю в базу, а там делит помечу как $delete
            --><?php echo $bookmarks_id['id']; ?>

                                <?php
                                $bookmarks_connect = mysqli_query($connection, "SELECT * FROM bookmarks");

                                while ($bookmarks_id = mysqli_fetch_assoc($bookmarks_connect)) {
                                ?>

                                <a onclick="if(confirm('Удалить книгу из закладок?')){alert('Книга удалена!'); return true}else{return false}" href="?del_id=<?php echo $bookmarks_id['id']; ?>">удалить id <?php echo $bookmarks_id['id']; ?></a> /


                               <?php } ?>
                            </section>

                        </li>
                    <?php } ?>
                </ul>
            </section>
        </section>
    </section>

    <section style="clear: both;"></section>


    <?php } else { ?>
        Закладки доступны только зарегистрированным пользователям! <a href="/admin/register.php">Регистрация</a>
    <?php } ?>



</section>


<section style="clear: both;"> </section>

<? include "includes/footer.php" ?>

</body>
</html>