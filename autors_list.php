<?php
require_once  "includes/config.php";
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $config['title']; ?></title>
    <meta name="viewport" content="width=device-width">
    <?php include "includes/head.php" ?>



</head>
<body>

<? include "includes/header.php" ?>

<section class="pattern">
    <section class="row">
        <section class="page_name">
            <h1><a href="/"><?php echo $config['title']; ?></a></h1>
        </section>
    </section>
</section>



<section class="container-content">
    <section class="container-content-w">
        <?php  include "includes/sidebar.php"; ?>
    <section class="content">

        <section class="latest_books">
            <h4>Все авторы</h4>
            <ul>
                <?

                $per_autor = 4;  /// переменная на странице = 4 книги (пагинация)
                $autor = 1;
                $offset = 0;

                if (isset($_GET['autor']) )
                {
                    $book = (int) $_GET['autor'];
                }

                $total_count_q = mysqli_query($connection, "SELECT COUNT(id) AS total_count FROM autors"); // извлекаем колчество книг, страниц
                $total_count = mysqli_fetch_assoc($total_count_q);
                $total_count = $total_count['total_count'];

                $total_autors = ceil($total_count / $per_autor ); // делим все количество книг на количество отображаемых книг
                /// и получаем количество страниц с книгами пагинации
                if ( autor <= 1 || autor >  $total_autors )    /// если книг меньше или равно 1 или больше всего количества книг, то
                {
                    $autor = 1;  /// тогда равно 1, т.е. если пользователь в ссылке написал 100, то перемещаем его на 1, потому что 100 книг или страниц у нас нет
                }

                if ($autor != 0) /// если страница не равна 0, то
                {

                    $offset = $per_autor * $autor; // это мы делаем только для не первой страницы, для любой но не первой

                }


                $offset = ($per_autor * $autor) - $per_autor;  /// получаем оффсет
                /// например для 1й страницы например 4 * 1 = 4 - 4 = 0
                /// получаем  для 2й  страницы например 4 * 2 = 8 - 4 = 4
                /// получаем  для 3й  страницы например 4 * 3 = 12 - 4 = 8



                $autors = mysqli_query($connection, "SELECT * FROM autors ORDER BY 'id' DESC LIMIT $offset,$per_autor");
                ///offset дает нам сдвиг, а $per_book - дает количество возврата книг


                $books_exist = true;

                if (mysqli_num_rows($autors) <= 0)
                {
                    echo 'Нету авторов!';
                    $books_exist = false;
                }
                ?>

                <? while ( $latest_autor = mysqli_fetch_assoc($autors) ) { ?>
                    <li>
                        <section class="imagebg" >
                            <a href="/autor.php?id=<? echo $latest_autor['id']; ?>"><img src="/upload/images/autors/<? echo $latest_autor['img_autor']; ?>" /></a>
                        </section>
                        <section class="name_book">
                            <a href="/autor.php?id=<? echo $latest_autor['id']; ?>"><? echo $latest_autor['name_autor']; ?></a>
                        </section>
                        <? $autor_cat = false;
                        foreach ( $categorie as $cat )
                        {

                            if( $cat['id']  == $latest_autor['id_books'] )
                            {
                                $autor_cat = $cat;
                                break;
                            }
                        } ?>

                        <section class="preview_description">
                            <? echo mb_substr(strip_tags($latest_autor['biograhy']), 0 , 120, 'utf-8'); ?>
                        </section>

                    </li>


                <? } ?>


            </ul>
        </section>
        <? if ($books_exist == true)
        {
            echo '<div class="paginator">';
            if ( $book >1 )
            {
                echo '<a href="/books_list.php?book='.($book - 1).'">Предыдущая страница</a>';
            }
            if ( $book < $total_books )
            {
                echo '<a href="/books_list.php?book='.($book + 1).'">Следующая страница</a>';
            }
            echo '</div>';
        }
        ?>


        <section style="clear: both;"></section>




</section>
        <section style="clear: both;"> </section>

    </section>
</section>


<section style="clear: both;"> </section>

<? include "includes/footer.php" ?>

</body>
</html>