<?php
require_once  "includes/config.php";
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $config['title']; ?></title>
    <meta name="viewport" content="width=device-width">
    <?php include "includes/head.php" ?>



</head>
<body>

<? include "includes/header.php" ?>

<section class="pattern">
    <section class="row">
        <section class="page_name">
            <h1><a href="/"><?php echo $config['title']; ?></a></h1>
        </section>
    </section>
</section>

<?php

session_start();
session_destroy();
header ('Location: http://lib.php2.ru');
exit;

?>

