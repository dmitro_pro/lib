
<?php
require_once  "../includes/config.php";
?>
<!DOCTYPE html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $config['title']; ?></title>
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" type="text/css" href="/assets/admin_css/admin_css.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- Подключаем Bootstrap CSS -->
    <link rel="stylesheet" href="/admin/assets/bootstrap/docs/dist/css/bootstrap.min.css" >
    <!-- Подключаем jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Подключаем Bootstrap JS -->
    <script src="/admin/assets/bootstrap/docs/dist/js/bootstrap.min.js"></script>
</head>

<body style="padding-top: 40px;
    padding-bottom: 40px;
    background-color: #f5f5f5;">
