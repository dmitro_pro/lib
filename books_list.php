<?php
require_once  "includes/config.php";
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?php echo $config['title']; ?></title>
    <meta name="viewport" content="width=device-width">
    <?php include "includes/head.php" ?>

    <?php


    if (isset($_POST['do_id'])) {

        $book_bookmarks = $_GET['book_id']; // id книги

        $user_id = mysqli_query($connection, "SELECT * FROM users WHERE login = '".$_SESSION['login']."' "); /// подключаем таблицу пользователей

        $user_id_q = mysqli_fetch_assoc($user_id); /// извлекаем данные из таблицы

        $user_id_q_have = $user_id_q['id']; // id авторизованного пользователя

        // проверяем, не добавляли ли мы ранее эту книгу в закладки
        $bookmark = mysqli_query($connection, "SELECT * FROM bookmarks WHERE user_id = '".$user_id_q_have."' AND book_id = '".$book_bookmarks."'");
        $bookmark_row = mysqli_num_rows($bookmark);
        if ($bookmark_row > 0)
        {
            echo 'Эта книга уже есть в закладках';
        }
        else
        {
            mysqli_query($connection, "INSERT INTO bookmarks (user_id, book_id) VALUES ('$user_id_q_have', '$book_bookmarks')");
        }




    } //else {

    //  echo 'Ошибка';
    //}



    ?>

</head>
<body>

<? include "includes/header.php" ?>
<?php session_start(); ?>
<section class="pattern">
    <section class="row">
        <section class="page_name">
            <h1><a href="/"><?php echo $config['title']; ?></a></h1>
        </section>
    </section>
</section>




<section class="container-content">
    <section class="container-content-w">
    <section class="content">

        <section class="latest_books">
            <h4>Все книги</h4>
            <ul>
                <?
                $books = mysqli_query($connection, "SELECT * FROM books ORDER BY 'id' DESC LIMIT 9");

                if(isset($_SESSION['login'])) {
                    $user_id = mysqli_query($connection, "SELECT * FROM users WHERE login = '".$_SESSION['login']."' "); /// подключаем таблицу пользователей
                    $user_id_q = mysqli_fetch_assoc($user_id); /// извлекаем данные из таблицы
                    $user_id_q_have = $user_id_q['id']; // id авторизованного пользователя
                }
                ?>

                <?php

                $books = mysqli_query($connection, "SELECT * FROM books WHERE id_categories = " .  $_GET['categorie']);




                    while ( $latest_book = mysqli_fetch_assoc($books) ) { ?>
                        <?php
                        // проверяем, не добавляли ли мы ранее эту книгу в закладки причем проверка идет по каждой книге внутри цикла
                        $bookmark = mysqli_query($connection, "SELECT * FROM bookmarks WHERE user_id = '".$user_id_q_have."' AND book_id = '".$latest_book['id']."'");
                        $bookmark_row = mysqli_num_rows($bookmark);
                        ?>
                        <li>
                            <section class="imagebg" >
                                <a href="/book.php?id=<? echo $latest_book['id']; ?>"><img src="/upload/images/books/<? echo $latest_book['img_book']; ?>" /></a>
                            </section>
                            <section class="name_book">
                                <a href="/book.php?id=<? echo $latest_book['id']; ?>"><? echo $latest_book['name_book']; ?></a>
                            </section>

                            <section class="preview_description">
                                <? echo mb_substr(strip_tags($latest_book['description_book']), 0 , 120, 'utf-8'); ?>
                            </section>
<hr />
                            <?php if(isset($_SESSION['login'])) { ?>

                                <form method="POST" action="/index.php?book_id=<?php echo $latest_book['id']; ?>" >

                                    <?php if ($bookmark_row > 0): ?>
                                        Книга в закладках
                                    <?php else: ?>
                                        <input type="submit" name="do_id" value="Добавить в закладки" />
                                    <?php endif; ?>
                                </form>





                            <?php } else { ?>
                                Чтобы добавить книгу в закладки - <a target="_blank" href="/admin/register.php">зарегистрируйтесь</a>
                            <?php } ?>
                        </li>


                    <? } ?>





            </ul>
        </section>


        <section style="clear: both;"></section>




    </section>
        <section style="clear: both;"></section> </section>

    </section>
</section>


<section style="clear: both;"> </section>

<? include "includes/footer.php" ?>

</body>
</html>